public class Dog {
    public String species;
    public int age;
    public double size;
    public void run(){
        System.out.println("The " + species + " runs for " + 10 * size + " meters!");
    }
    public void jump(){
        System.out.println("The " + species + " jumps for " + 2 * age + " centimeters!" );
    }
}

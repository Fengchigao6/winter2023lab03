import java.util.Scanner;
public class NationalPark {
    public static void main(String[] args) {
        Dog[] aPackOfDog = new Dog[4];
        Scanner sc =new Scanner(System.in);
        for (int i = 0; i < 4; i++) {
            aPackOfDog[i] =new Dog();
            System.out.println("Please enter your dog's species");
            aPackOfDog[i].species = sc.nextLine();
            System.out.println("Please enter the dog's age");
            aPackOfDog[i].age = sc.nextInt();
            System.out.println("Please enter the dog's size in meters");
            aPackOfDog[i].size = sc.nextDouble();
			sc.nextLine();
        }
        System.out.println("The dog is " + aPackOfDog[3].species+", it is " + aPackOfDog[3].age + " years old,  it is " + aPackOfDog[3].size + " meters.");
        aPackOfDog[0].jump();
        aPackOfDog[0].run();
    }
}
